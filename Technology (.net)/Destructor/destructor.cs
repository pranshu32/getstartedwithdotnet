using System;

class Example
{
    public Example()
    {
        Console.WriteLine("Constructor is called!");
    }
    // destructor
    ~Example()
    {
        // destructor is called automatically at the end of program! 
        // TO DO: Cleanup of unmanaged objects.
        Console.WriteLine("Destructor is called!");
    }
}
class Program
{
    static void Main()
    {
        Example e = new Example();
        Console.WriteLine("Program executed");
    }
}
