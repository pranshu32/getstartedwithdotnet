﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var customer = new Customer(1);
            customer.Orders.Add(new Order());
            customer.Orders.Add(new Order());

            customer.Promote(); // 0
             // To overcome this problem of 0 we use "readonly" modifier!
            Console.WriteLine(customer.Orders.Count);
        }
    }
}
