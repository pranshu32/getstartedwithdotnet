﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class Customer
    {
        public int Id;
        public string Name;
        // public List<Order> Orders = new List<Order>();
        public readonly List<Order> Orders = new List<Order>(); // * both are same but be consistent! 
        /*
        public Customer() // * both are same but be consistent!
        {
            Orders = new List<Order>();
        }
        */
        public Customer(int id)
        {
            this.Id = id;
        }
        public Customer(int id, string name)
            : this(id)
        {
            this.Name = name;
        }
        public void Promote() // mistake senerio
        {
            // demo code
            // now since above we have used readonly Modifiers, below line will throw error!
            // Orders = new List<Order>();
        }

    }
}
