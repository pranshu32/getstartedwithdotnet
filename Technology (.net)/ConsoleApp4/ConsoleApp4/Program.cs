﻿using System; 

namespace ConsoleApp4
{
    // Types of Constructor - default, static, parameterized, private
    class Student
    {
        public int StudentId { get; set; }
        public string Name { get; set; }
        public string College { get; set; }
        // default Constructor
        public Student()
        {
            College = "PSIT";
        }
        // parameterized Constructor
        public Student(int studentId, string name, string college)
        {
            this.StudentId = studentId;
            this.Name = name; 
            this.College = college;
            // if both the names are different then this is optional
        }
        // private Constructor - it restricts the class external instantiation,
        // but in nested class we can create instance of this class!
        
        // in C# 1.X there was no static class,
        // hence we used the private constructor
        // to prevent a class external instantiation   
    
        // used to implement singleton pattern
        // i.e. a single instance for a class
    
        // A class can have multiple private Constructor and public Constructors
    }
    class Program
    {
        static void Main(string[] args)
        {
            Student s = new Student();
            Console.WriteLine("StudentId : {0}, Name : {1}, College : {2}", s.StudentId, s.Name, s.College);
            // StudentId : 0, Name : , College : PSIT
            Student s1 = new Student(190, "Pranshu", "PSIT");
            Console.WriteLine("StudentId : {0}, Name : {1}, College : {2}", s1.StudentId, s1.Name, s1.College);

        }
    }
}