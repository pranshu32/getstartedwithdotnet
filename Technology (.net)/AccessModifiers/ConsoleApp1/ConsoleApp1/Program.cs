﻿using System;

namespace ConsoleApp1
{
    public class Person
    {
        private DateTime _birthdate;
        public void SetBirthdate(DateTime birthdate)
        {
            _birthdate = birthdate;
        }
        public DateTime GetBirthdate()
        {
            return _birthdate;
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            var person = new Person();
            // YYYY DD MM
            person.SetBirthdate(new DateTime(2001, 7, 10));
            Console.WriteLine(person.GetBirthdate());
        }
    }
}
