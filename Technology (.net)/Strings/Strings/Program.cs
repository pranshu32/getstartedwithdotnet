﻿using System;
using System.IO;
using System.Net;

namespace Strings
{
    class stringManipulation
    {
        public static void Examples()
        {
            string s = "The quick brown fox jumped over the lazy dog."; 
            Console.WriteLine(s);
            Console.WriteLine(string.Format("{1} = {0}", "first", "second"));
            Console.WriteLine(string.Format("{0:C}", 123.45));
            Console.WriteLine(string.Format("{0:N}", 123456789));
            Console.WriteLine(string.Format("Percentage : {0:P} ", .9969));
            Console.WriteLine(string.Format("Phone Number : {0:(+91)-##########}", 9807643922));
            Console.WriteLine(s.ToUpper());
            Console.WriteLine(s.ToLower());
            Console.WriteLine(s.Replace(" ", "-"));
            Console.WriteLine(s.Remove(5, 15));
            Console.WriteLine(s.Substring(4, 12));
            string trimString = string.Format("Length before : {0} -- Length after : {1}", s.Length, s.Trim().Length);
            Console.WriteLine(trimString);

            Console.WriteLine("\n\n\n");

            // fetch the webpage html content !
            WebClient client = new WebClient();
            string response = client.DownloadString("https://www.stackoverflow.com");
            Console.WriteLine(response);
            File.WriteAllText(@"D:\Technology (.net)\Strings\website.txt", response);
            Console.WriteLine("\n-------------------------------------------------------\n");
            Console.WriteLine("Done successfully!");
        }
    }

    class Program
    {
        public static void Main(string[] args)
        {
            stringManipulation.Examples();
        }
    }
}