﻿using System;
using ConsoleApp2;

namespace ConsoleApp3
{
    public class ConsoleApp3DerrivedClass: ConsoleApp2BaseClass
    {
        public void TestAccessInDerrivedClass()
        {

            Console.WriteLine(protectedVariable);
            Console.WriteLine(protectedInternalVariable);
            Console.WriteLine(publicVariable);
            // derrived class is in different namespace hence inaccessible  
            // Console.WriteLine(privateVariable); // not accessible
            // Console.WriteLine(internalVariable); // not accessible
        }
    }

    public class ConsoleApp3OtherClass
    {
        public void TestAccess()
        {
            ConsoleApp2BaseClass objBase = new ConsoleApp2BaseClass();
            // not accessible
            // Console.WriteLine(objBase.privateVariable); 
            // Console.WriteLine(objBase.protectedVariable); 
            // accessible!
            // Console.WriteLine(objBase.internalVariable);
            // Console.WriteLine(objBase.protectedInternalVariable);
            Console.WriteLine(objBase.publicVariable);

        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleApp3DerrivedClass objDerrived = new ConsoleApp3DerrivedClass();
            objDerrived.TestAccessInDerrivedClass(); // protected, protected internal, public

            Console.WriteLine("\nTest Access - ");
            objDerrived.TestAccess();
            // all are accessible
            // private, protected, internal, protected, internal, public

            Console.WriteLine("\nOther  Class - ");
            ConsoleApp3OtherClass objOther = new ConsoleApp3OtherClass();
            objOther.TestAccess(); // public is only accessible

        }
    }
}