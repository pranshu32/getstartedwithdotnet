﻿using System;
using System.Collections;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

//namespace ConsoleApp2

//{
//    class Student
//    {
//        public int StudentId { get; set; }
//        public string Name { get; set; }
//        public string Address { get; set; }
//        public void ShowDetails()
//        {
//            Console.WriteLine("Student Information");
//            Console.WriteLine("StudentId : {0}, Name : {1}, Address : {2}", StudentId, Name, Address);
//        }
//    }
//    class Program
//    {
//        static void Main(string[] args)
//        {
//            Student s = new Student();
//            s.StudentId = 1;
//            s.Name = "Pranshu";
//            s.Address = "Gurugram";
//            s.ShowDetails();
//        }
//    }
//}


//// Access modifiers suppport Abstraction
//// private, public, protected, Internal, Protected, Internal

//{
//    public class ConsoleApp2BaseClass
//    {
//        private string privateVariable = "private";
//        protected string protectedVariable = "protected";
//        internal string internalVariable = "internal";
//        protected internal string protectedInternalVariable = "protected internal";
//        public string publicVariable = "public";

//        public void TestAccess()
//        {
//            // within the same class all members can be accessed.
//            Console.WriteLine(privateVariable);
//            Console.WriteLine(protectedVariable);
//            Console.WriteLine(internalVariable);
//            Console.WriteLine(protectedInternalVariable);
//            Console.WriteLine(publicVariable);
//        }
//    }
//    public class ConsoleApp2OtherClass
//    {
//        public void TestAccess()
//        {
//            ConsoleApp2BaseClass objBase = new ConsoleApp2BaseClass();
//            // not accessible
//            // Console.WriteLine(objBase.privateVariable); 
//            // Console.WriteLine(objBase.protectedVariable); 
//            // accessible!
//            Console.WriteLine(objBase.internalVariable);
//            Console.WriteLine(objBase.protectedInternalVariable);
//            Console.WriteLine(objBase.publicVariable);
//        }
//    }
//    // bydefault Access Pacifier is Internal
//    public class ConsoleApp2DerrivedClass : ConsoleApp2BaseClass
//    {
//        public void TestAccessInDerrivedClass()
//        {
//            //Console.WriteLine(privateVariable); //not accessible
//            Console.WriteLine(protectedVariable);
//            Console.WriteLine(internalVariable);
//            Console.WriteLine(protectedInternalVariable);
//            Console.WriteLine(publicVariable);
//        }
//    }

//    class Program
//    {
//        static void Main(string[] args)
//        {
//            Console.WriteLine("Base Class -");
//            ConsoleApp2BaseClass objBase = new ConsoleApp2BaseClass();
//            objBase.TestAccess();

//            Console.WriteLine("\nDerrived Class -");

//            ConsoleApp2DerrivedClass objDerrived = new ConsoleApp2DerrivedClass();
//            // objDerrived.TestAccess(); // we can call parent class method with the help of child class object
//            objDerrived.TestAccessInDerrivedClass();

//            Console.WriteLine("\nOther Class - ");

//            ConsoleApp2OtherClass objOther= new ConsoleApp2OtherClass();
//            // objDerrived.TestAccess(); // we can call parent class method with the help of child class object
//            objOther.TestAccess();

//        }
//    }
//}
namespace ConsoleApp2
{
    public class dell
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("This is a console Application");
            Console.WriteLine("Please enter l n:");
            string sal1 = Console.ReadLine();
            int sal2 = System.Convert.ToInt32(sal1, 10);
            sal2 = sal2 * sal2;
            Console.WriteLine(sal2);
            Console.Read();
        }
    }

}

