﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConventionalRouting.Models;


// localhost:xxxx/student/actionMethod 


namespace ConventionalRouting.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult GetAllStudents()
        {
            var students = Students();
            return View(students); // strongly type binding
        }
        
        // How to use this  -> // student/GetStudents?id=1/2/3/...
        public ActionResult GetStudents(int id)
        {
            var student = Students().FirstOrDefault(x=>x.Id==id);
            return View(student);   // strongly type binding
        }
        public ActionResult GetStudentAddress(int id)
        {
            var studentAddress = Students().Where(x => x.Id == id).Select(x => x.Address);
            return View(studentAddress);    // strongly type binding
        }
        private List<Student> Students() 
        {
            return new List<Student>()
            {
                new Student()
                {
                    Id=1,
                    Name="Student1",
                    Class="First",
                    Address=new Address()
                    {
                        Address1="This is student1 address",
                        City = "City1",
                        HomeNumber="1234567890"
                    }
                },
                new Student()
                {
                    Id=2,
                    Name="Student2",
                    Class="Second",
                    Address=new Address()
                    {
                        Address1="This is student2 address",
                        City = "City2",
                        HomeNumber="0987654321"
                    }
                },
                new Student()
                {
                    Id=3,
                    Name="Student3",
                    Class="Third",
                    Address=new Address()
                    {
                        Address1="This is student3 address",
                        City = "City3",
                        HomeNumber="0987656789"
                    }
                },
                new Student()
                {
                    Id=4,
                    Name="Student4",
                    Class="Fourth",
                    Address=new Address()
                    {
                        Address1="This is student4 address",
                        City = "City4",
                        HomeNumber="7654322346"
                    }
                },
                new Student()
                {
                    Id=1,
                    Name="Student5",
                    Class="Fiveth",
                    Address=new Address()
                    {
                        Address1="This is student5 address",
                        City = "City5",
                        HomeNumber="98765434567"
                    }
                },
                new Student()
                {
                    Id=1,
                    Name="Student6",
                    Class="Sixth",
                    Address=new Address()
                    {
                        Address1="This is student6 address",
                        City = "City6",
                        HomeNumber="5432345677"
                    }
                }
            };
        }
    }
}