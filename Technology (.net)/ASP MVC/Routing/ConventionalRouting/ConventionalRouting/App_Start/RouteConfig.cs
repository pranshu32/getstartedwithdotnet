﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ConventionalRouting
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}"); // used for ignoringPath

            // localhost:xxxx/students
            routes.MapRoute(
                name: "allStudents",
                url: "students",
                defaults: new { controller = "Student", action = "GetAllStudents" }
            );

            // localhost:xxxx/students/1,2,3,4,5,6,....
            routes.MapRoute(
                name: "student",
                url: "students/{id}",  // { } are used to enter something at runtime!
                defaults: new { controller = "Student", action = "GetStudents" }
            );

            // localhost:xxxx/students/1,2,3,4,.../address
            routes.MapRoute(
                name: "studentAddress",
                url: "students/{id}/Address",  // { } are used to enter something at runtime!
                defaults: new { controller = "Student", action = "GetStudentAddress" },
                constraints: new { id = @"\d+"} 
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
