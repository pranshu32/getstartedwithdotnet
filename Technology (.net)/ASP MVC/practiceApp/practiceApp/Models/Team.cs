﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace practiceApp.Models
{
    public class Team: Player
    {
        public Player p1 { get; set; }
        public Player p2 { get; set; }
        public Player p3 { get; set; }
        public Player p4 { get; set; }
    }
}