﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace practiceApp.Models
{
    public class Player
    {
        public int Id { get; set; }
        public string IGN { get; set; }
        public string org { get; set; }
        public string playingStyle { get; set; }
        public string role { get; set; }
    }
}