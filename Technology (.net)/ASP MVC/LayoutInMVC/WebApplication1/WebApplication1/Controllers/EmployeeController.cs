﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class EmployeeController : Controller
    {
        // GET: Employee
        [Route("Employees")]  //mapped in routeconfig
        public ActionResult GetList()
        {
            // return View(); // in case of common LayoutView use this!
            // and remove the below line, and goto _ViewStart file
            // return View("GetList", "_Layout");
            return View();
        }

        [Route("NewEmployee")]
        public ActionResult AddEmployee()
        {
            //return View("AddEmployee", "_Layout");
            return View();
        }
    }
}