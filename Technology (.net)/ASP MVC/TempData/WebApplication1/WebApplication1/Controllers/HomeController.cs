﻿// TempData, Keep, Peek - 
// TempData internally uses session!
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            TempData["Key"] = "Data from Index Method!";
            return View();
        }
        public ActionResult Index2()
        {
            //ViewBag.MyKey=TempData["Key"];
            //// as soon as we refresh the Index2 page,
            ////  after the first time it will not contain the value!

            //# ViewBag.MyKey = TempData["Key"]; // reads the data
            //# TempData.Keep("Key"); // now it will hold the value for Index3

            // above two lines are used -
            // does the same work in 1 line

            // Session.Abandon(); // due to this tempdata willn't work 
            ViewBag.MyKey = TempData.Peek("Key");
            return View();
        }

        public ActionResult Index3()
        {
            //ViewBag.MyKey = TempData["Key"];
            //// here we can't access data due to the limitation of one life cycle! 
            ViewBag.MyKey = TempData["Key"];
            return View();
        }
    }
}