﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication3.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AboutUs()
        {
            return View();
        }
        public ActionResult WithoutView()
        {
            // way to call a customView but folder name is same
            // i.e. controller name is same
            return View("Rename");
        }
        public ActionResult DifferentView()
        {
            return View("~/Views/MyView/MyNewView.cshtml");
        }
    }
}