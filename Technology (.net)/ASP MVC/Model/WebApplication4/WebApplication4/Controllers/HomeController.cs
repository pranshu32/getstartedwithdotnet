﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication4.Models;

namespace WebApplication4.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            var data = GetEmployee();
            return View(data); // data object is passed in view 
        }
        private Employee GetEmployee()
        {
            return new Employee()
            {
                Id = 1,
                Name = "Pranshu",
                Country = "India"
            };
        }
    }
}