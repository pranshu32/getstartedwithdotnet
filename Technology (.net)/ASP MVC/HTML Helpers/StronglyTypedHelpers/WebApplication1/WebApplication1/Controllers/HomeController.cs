﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;


namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        // sending data from controller to view!
        public ActionResult Index()
        {
            Employee emp = new Employee()
            {
                Name = "Pranshu",
                Address = "this is my address!",
                IsEmployee = true
            };

            return View(emp);
        } 
        // Post Method using Attributes!
        [HttpPost]
        public ActionResult Index(Employee emp)
        {
            return View();
        }
    }
}