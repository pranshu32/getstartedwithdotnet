﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace TemplateHTMLHelpers.Models
{
    public class Employee
    {
        [Display(Name = "ID : ")]
        public int Id { get; set; }
        
        [Display(Name = "Please enter your name : ")] // if this is not provided, byDefault it takes propertyName as Name!
        public string Name { get; set; }
        
        [Display(Name = "Email : ")]
        public string Email { get; set; }
        
        [Display(Name="isOnline : ")]
        public bool IsOnline { get; set; }
  
        [Display(Name = "Date of  Birth : ")]
        
        [DataType(DataType.Date)] //its used because HTML has no builtin datetime,
                                  //hence to show date component we use this
        public DateTime Dob { get; set; }

        [DataType(DataType.Time)]
        public DateTime BirthTime { get; set; }
        
        [DataType(DataType.PhoneNumber)]
        public long PhoneNo { get; set; }
    }
}