﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using TemplateHTMLHelpers.Models;

namespace TemplateHTMLHelpers.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            Employee emp = new Employee()
            {
                Id = 1,
                Name = "Pranshu Gupta",
                Email = "pranshu.gupta@unthinkable.co",
                IsOnline = true,
                Dob = Convert.ToDateTime("10/07/2001") // mm DD YYY
            };
            return View(emp);
        }
        public ActionResult Edit()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Edit(Employee emp)
        { 
            return View(emp);
        }
    }
}