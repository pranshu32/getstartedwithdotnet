﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            List<Player> players = new List<Player>() {
                new Player()
                {
                    Id = 1,
                    Name = "GodLxJonathan",
                    Photo = "Godlike/Jonathan_Godlike.png",
                    Org = "GodLike Esports",
                    Role = "Assaulter"
                },
                new Player()
                {
                    Id = 2,
                    Name = "GodLxNeyooo",
                    Photo = "Godlike/Neyoo_Godlike.png",
                    Org = "GodLike Esports",
                    Role = "Assaulter"
                },
                new Player()
                {
                    Id = 3,
                    Name = "GodLxClutchgod",
                    Photo = "Godlike/ClutchGod_Godlike.jpg",
                    Org = "GodLike Esports",
                    Role = "IGL"
                },
                new Player()
                {
                    Id = 4,
                    Name = "GodLxZGOD",
                    Photo = "Godlike/ZGOD_Godlike.jpg",
                    Org = "GodLike Esports",
                    Role = "Support"
                },

                new Player()
                {
                    Id = 5,
                    Name = "PVxINDSnaxx",
                    Photo = "IND/Snax_Ind.jpg",
                    Org = "Insidious Esports",
                    Role = "Assaulter, Sniper"
                },
                new Player()
                {
                    Id = 6,
                    Name = "PVxINDSlayer",
                    Photo = "IND/Slayer_Ind.jpg",
                    Org = "Insidious Esports",
                    Role = "Sniper, IGL"
                },
                new Player()
                {
                    Id = 7,
                    Name = "PVxINDKratos",
                    Photo = "IND/Kratos_Ind.jpg",
                    Org = "Insidious Esports",
                    Role = "IGL"
                },
                new Player()
                {
                    Id = 8,
                    Name = "PVxINDDaljit",
                    Photo = "IND/Daljit_Ind.jpg",
                    Org = "Insidious Esports",
                    Role = "Sniper"
                },
            };
            ViewBag.players = players;
            return View();
        }

    }
}