﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace CustomValidationSample.Models
{
    public class Employee
    {
        [Required]
        public string Name { get; set; }

        [CustomizedValidation(ErrorMessage ="CustomErrorInAnnotation")]
        public string Message { get; set; }
    }
}