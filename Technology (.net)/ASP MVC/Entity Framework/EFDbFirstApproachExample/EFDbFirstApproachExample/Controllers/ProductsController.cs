﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EFDbFirstApproachExample.Models;

namespace EFDbFirstApproachExample.Controllers
{
    public class ProductsController : Controller
    {
        // GET: Products
        // Index(blank search parameter, SortColumn(byDefault sorts according to ProductName), IconClass(used font Awesome to display this icon small arrow), PageNo(bydefault page1 is displayed))
        public ActionResult Index(string search="", string SortColumn="ProductName", string IconClass="fa-sort-desc", int PageNo=1) // parameter name should be same as name used in form(index.cshtml of product!)
        {
            // EFDBFirstDatabaseEntities db = new EFDBFirstDatabaseEntities(); // have to close the connection manually if we use this
            // List<Product> products = db.Products.ToList(); // source of data for this application!,
            // displays everything inside the table
            using (var db = new EFDBFirstDatabaseEntities()) //automatically opens and close connection!
            {
                ViewBag.search = search; // to keep the search item displayed in search box 
                List<Product> products = db.Products.Where(temp => temp.ProductName.Contains(search)).ToList(); // retrieves info about product having id=1
                
                //------------------------------------------------------------------------------------------------

                // Sorting the data
                ViewBag.SortColumn = SortColumn;
                ViewBag.IconClass = IconClass;

                // Sorting according to ProductID
                if (ViewBag.SortColumn == "ProductID")
                {
                    if(ViewBag.IconClass == "fa-sort-asc")
                    {
                        products = products.OrderBy(x => x.ProductID).ToList();
                    }
                    else
                    {
                        products = products.OrderByDescending(x => x.ProductID).ToList();
                    }
                }

                // Sorting according to ProductName
                if (ViewBag.SortColumn == "ProductName")
                {
                    if (ViewBag.IconClass == "fa-sort-asc")
                    {
                        products = products.OrderBy(x => x.ProductName).ToList();
                    }
                    else
                    {
                        products = products.OrderByDescending(x => x.ProductName).ToList();
                    }
                }

                // Sorting according to Date of Purchase
                if (ViewBag.SortColumn == "DateOfPurchase")
                {
                    if (ViewBag.IconClass == "fa-sort-asc")
                    {
                        products = products.OrderBy(x => x.DateOfPurchase).ToList();
                    }
                    else
                    {
                        products = products.OrderByDescending(x => x.DateOfPurchase).ToList();
                    }
                }

                // Sorting according to Availability Status
                if (ViewBag.SortColumn == "AvailabilityStatus")
                {
                    if (ViewBag.IconClass == "fa-sort-asc")
                    {
                        products = products.OrderBy(x => x.AvailabilityStatus).ToList();
                    }
                    else
                    {
                        products = products.OrderByDescending(x => x.AvailabilityStatus).ToList();
                    }
                }

                // Sorting according to Category
                if (ViewBag.SortColumn == "CategoryID")
                {
                    if (ViewBag.IconClass == "fa-sort-asc")
                    {
                        products = products.OrderBy(x => x.Category.CategoryName).ToList();
                    }
                    else
                    {
                        products = products.OrderByDescending(x => x.Category.CategoryName).ToList();
                    }
                }

                // Sorting according to Brand
                if (ViewBag.SortColumn == "BrandID")
                {
                    if (ViewBag.IconClass == "fa-sort-asc")
                    {
                        products = products.OrderBy(x => x.Brand.BrandName).ToList();
                    }
                    else
                    {
                        products = products.OrderByDescending(x => x.Brand.BrandName).ToList();
                    }
                }

                //-------------------------------------------------------------------------------------------------

                // Paging 
                int NoOfRecordsPerPage = 5;
                int NoOfPages = (int)Math.Ceiling(Convert.ToDouble(products.Count)/Convert.ToDouble(NoOfRecordsPerPage));
                int NoOfRecordsToSkip = (PageNo - 1) * NoOfRecordsPerPage;
                ViewBag.PageNo = PageNo;
                ViewBag.NoOfPages = NoOfPages;
                products = products.Skip(NoOfRecordsToSkip).Take(NoOfRecordsPerPage).ToList();

                return View(products);
            }
        }

        // retrieving single record!
        public ActionResult Details(long id)
        {
            Product p;
            using (var db = new EFDBFirstDatabaseEntities())
            {
                p = db.Products.Where(temp => temp.ProductID == id).FirstOrDefault(); // retrieves info about product having id=1
            }
            return View(p);
        }

        // -------------------------------------creating record --------------------------------------------------------------------------------
       
        public ActionResult Create()
        {
            using (EFDBFirstDatabaseEntities db = new EFDBFirstDatabaseEntities())
            {
                ViewBag.Categories = db.Categories.ToList();
                ViewBag.Brands = db.Brands.ToList();
                return View();
            }
        }

        [HttpPost]
        public ActionResult Create(Product p)
        {
            using (var db = new EFDBFirstDatabaseEntities())
            {
                if (Request.Files.Count >= 1)
                {
                    var file = Request.Files[0];
                    var imgBytes = new Byte[file.ContentLength - 1];
                    file.InputStream.Read(imgBytes, 0, file.ContentLength);
                    var base64String = Convert.ToBase64String(imgBytes, 0, imgBytes.Length);
                    p.Photo = base64String;
                }
                db.Products.Add(p); // adding the info received to the db!
                db.SaveChanges(); //Saves the changes in the db
            }
            return RedirectToAction("Index");
        }

        // -------------------------------------updating existing Record!-------------------------------------------------------------------------

        public ActionResult Edit(long id)
        {
            using (var db= new EFDBFirstDatabaseEntities())
            {
                Product existingProduct = db.Products.Where(temp => temp.ProductID == id).FirstOrDefault();
                ViewBag.Categories = db.Categories.ToList(); // to make the dynamic dropdown menu
                ViewBag.Brands = db.Brands.ToList();
                return View(existingProduct);
            }
        }

        [HttpPost]
        public ActionResult Edit(Product p)
        {
            using (var db = new EFDBFirstDatabaseEntities())
            {
                Product existingProduct = db.Products.Where(temp => temp.ProductID == p.ProductID).FirstOrDefault();
                existingProduct.ProductName = p.ProductName;
                existingProduct.Price = p.Price;
                existingProduct.DateOfPurchase = p.DateOfPurchase;
                existingProduct.CategoryID = p.CategoryID;
                existingProduct.BrandID = p.BrandID;
                existingProduct.AvailabilityStatus = p.AvailabilityStatus;
                existingProduct.Active = p.Active;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
        }

        public ActionResult Delete(long id)
        {
            using (var db = new EFDBFirstDatabaseEntities())
            {
                Product existingProduct = db.Products.Where(temp => temp.ProductID == id).FirstOrDefault();
                return View(existingProduct);
            }
        }

        [HttpPost]
        public ActionResult Delete(long id, Product p)
        {
            using (var db = new EFDBFirstDatabaseEntities())
            {
                Product existingProduct = db.Products.Where(temp => temp.ProductID == p.ProductID).FirstOrDefault();
                db.Products.Remove(existingProduct);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
        }
    }
}