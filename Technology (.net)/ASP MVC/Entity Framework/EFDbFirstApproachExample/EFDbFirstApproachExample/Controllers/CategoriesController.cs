﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EFDbFirstApproachExample.Models;

namespace EFDbFirstApproachExample.Controllers
{
    public class CategoriesController : Controller
    {
        // GET: Categories
        public ActionResult Index()
        {
            EFDBFirstDatabaseEntities db = new EFDBFirstDatabaseEntities(); // to access dbsets, creating this object
            
            List <Category> categories = db.Categories.ToList(); // makes entity framework to start the process
            
            return View(categories); // it must be strongly typed so it can be retrieved
        }
    }
}