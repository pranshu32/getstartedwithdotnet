﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace CodeFirstApproachExample.Models
{
    public class Products
    {
        [Key]
        public int ProductID { get; set; }
        [Required(ErrorMessage ="Product Name Required!")]
        [Display(Name = "Product Name")]
        public string ProductName { get; set; }

        [Required(ErrorMessage ="Price is Required!")]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "Quantity is Required!")]
        public int Qty { get; set; }
        public string Remarks { get; set; }
    }
    public class EFCodeFirstEntities : DbContext
    {
        public DbSet<Products> products { get; set; }
    }
}