﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            List<Employee> emp = new List<Employee>()
            {
                new Employee()
                {
                    Id=1,
                    Name="A",
                    Email="a@gmail.com", 
                    Description="Description for a"
                },
                 new Employee()
                {
                    Id=2,
                    Name="B",
                    Email="b@gmail.com",
                    Description="Description for b"
                },
                  new Employee()
                {
                    Id=3,
                    Name="C",
                    Email="c@gmail.com",
                    Description="Description for c"
                },
                   new Employee()
                {
                    Id=4,
                    Name="D",
                    Email="d@gmail.com",
                    Description="Description for d"
                }
            };
            return View(emp);
        }
    }
}