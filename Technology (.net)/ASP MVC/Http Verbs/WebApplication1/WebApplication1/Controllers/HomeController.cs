﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult ViewProfile()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateProfile()
        {
            return View();
        }

        [HttpPut]
        public ActionResult UpdateProfile()
        {
            return View();
        }

        [HttpDelete]
        public ActionResult DeleteProfile()
        {
            return View();
        }
    }
}