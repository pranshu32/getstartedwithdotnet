﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebAppUsingList.Models
{
    public class Player
    {
        [Required(ErrorMessage ="ID cannot be empty")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Name cannot be empty")]
        [Display(Name="PlayerName : ")]
        public string Name { get; set; }

        [Display(Name = "Game : ")]
        [Required(ErrorMessage = "Game cannot be empty")]
        public string Game { get; set; }

        [Display(Name = "IGN : ")]
        [Required(ErrorMessage = "IGN cannot be empty")]
        public string IGN { get; set; }

        [Display(Name = "Organisation : ")]
        public string Organisation { get; set; }

        [Display(Name = "Role : ")]
        public string Role { get; set; }

        public string ImgPath { get; set; }
        public HttpPostedFileBase[] Files { get; set; }

    }
}