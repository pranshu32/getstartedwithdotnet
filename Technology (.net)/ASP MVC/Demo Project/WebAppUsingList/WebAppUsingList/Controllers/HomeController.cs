﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAppUsingList.Models;


namespace WebAppUsingList.Controllers
{
    [HandleError]
    public class HomeController : Controller
    {
        public static class Globals
        {
            public static List<Player> PlayerList = new List<Player>();
        }
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AddPlayer()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddPlayer(Player player)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var imgPath = "";
                    var image = "";

                    foreach(HttpPostedFileBase file in player.Files)
                    {
                        // checking whether file is available to save
                        if(file!=null && file.ContentType.Contains("image"))
                        {
                            var InputFileName = Path.GetFileName(file.FileName);
                            var path = Server.MapPath("~/Content/PlayerImages");
                            image = "~/Content/PlayerImages/" + InputFileName;
                            imgPath = Path.Combine(path, InputFileName);
                            // saves the file to server folder
                            file.SaveAs(imgPath);
                        }
                    }
                    ViewBag.UploadStatus = player.Files.Count().ToString() + "Image uploaded successfully!";
                    player.ImgPath = image;
                    Globals.PlayerList.Add(player);
                    
                }
            }
            catch (Exception e)
            {
                ViewBag.List = "List Empty due to " + e;
            }
            return RedirectToAction("ShowPlayer");
        }
        public ActionResult ShowPlayer()
        {
            ViewBag.List = Globals.PlayerList;
            return View();
        }

        public ActionResult DeletePlayer()
        {
            return View();
        }

        [HttpPost]
        public ActionResult DeletePlayer(int id)
        {
            var obj = Globals.PlayerList.Where(x => x.Id == id).FirstOrDefault();
            if (obj != null)
            {
                Globals.PlayerList.Remove(obj);
                ViewBag.Message = "Item Deleted";
            }
            else
            {
                ViewBag.Message = string.Format("This Player of ID : {0} Not Found", id);
            }
            return View();
        }
        public ActionResult EditFlag()
        {
            return View();
        }
        [HttpPost]
        public ActionResult EditFlag(int id)
        {
            TempData["Id"] = id;
            var info = Globals.PlayerList.Where(x => x.Id == id).FirstOrDefault();
            if (info != null)
            {
                ViewBag.response = "Player Found!";
                return RedirectToAction("EditDetails");
            }
            else
            {
                ViewBag.response = "Player Not Found!!!";
                return View();
            }
        }
        public ActionResult EditDetails()
        {
            return View();
        }
        [HttpPost]
        public ActionResult EditDetails(Player player)
        {
            var id = TempData["Id"].ToString();
            var info = Globals.PlayerList.Where(x => x.Id == int.Parse(id)).FirstOrDefault();

            if (player.Name != null)
                info.Name = player.Name;

            if (player.IGN != null)
                info.IGN = player.IGN;
            
            if (player.Organisation != null)
                info.Organisation = player.Organisation;

            if (player.Role != null)
                info.Role = player.Role;

            if (player.Game != null)
                info.Game = player.Game;


            return RedirectToAction("ShowPlayer");
        }
    }
}