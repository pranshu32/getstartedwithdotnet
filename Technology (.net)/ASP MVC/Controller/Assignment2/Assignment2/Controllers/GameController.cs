﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Assignment2.Controllers
{
    public class GameController : Controller
    {
        public string Name()
        {
            return "Welcome to the GhostHub";
        } 
        public string FullName(string first = "null", string last = "null")
        {
            string ans = "";
            if (first!="null" && last!="null")
                ans = "Your first name is = " + first + " and " + "last name is = " + last;
            else if (first!="null")
                ans = "Your first name is = " + first;
            else
                ans = "Your last name is = " + last;
            return ans;           
        }
    }
}