﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication2.Controllers
{
    public class EmployeeController : Controller
    {
        
        public string EmployeeProfile(int id)
        {
            string profile = string.Empty;
            if (id == 1)
                profile = "Profile 1";
            else if (id == 2)
                profile = "Profile 2";
            else
                profile = "No Record Found!";
            return profile;
        }
        /*
        public string Address(int id, string department)
        {
            return "id : " + id + " dept : " + department;
        }
        */
        public string Address(int id, int? code = null)
        {
            return "id : " + id + " , " + "code = " + code;
        }
    }
}