﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication2.Controllers
{
    // make a call
    // Syntax : YourDomain/ControllerName/ActionMethodName
    // Ex :     localhost:xxxx/Home/Index
    // Ex :     localhost:xxxx/Home/Name
    public class HomeController : Controller
    {
        public string Index()
        {
            return "Hello from GhostHub";
        }
        public string Name()
        {
            return "My name is Ghost";
        }
    }
}