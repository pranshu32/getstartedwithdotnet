﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataFromViewToController.Models;

namespace DataFromViewToController.Controllers
{
    public class HomeController : Controller
    {
        // GET: Index
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet] // get is not preffered as data passed is showed in the url!
        // here parameters name are same as used in HtmlHelpers! otherwise it'll not work!
        public string GetUsingParameters(string firstName, string lastName)
        {
            return "From parameters - " + firstName + " " + lastName;
        }

        [HttpPost]
        // here parameters name are same as used in HtmlHelpers! otherwise it'll not work!
        public string PostUsingParameters(string firstName, string lastName)
        {
            return "From parameters - " + firstName + " " + lastName;
        }

        [HttpPost]
        public string PostUsingRequest()
        {
            // string anyvariableName = Request["sameNameAsinHtmlHelper"]
            string firstName = Request["firstName"]; // Request Object contains all info from Post!
            string lastName = Request["lastName"];
            return "From Request - " + firstName + " " + lastName;

        }

        [HttpPost]
        public string PostUsingFormCollection(FormCollection form)
        {
            // string anyvariableName = Request["sameNameAsinHtmlHelper"]
            string firstName = form["firstName"]; // Request Object contains all info from Post!
            string lastName = form["lastName"];
            return "From FormCollection - " + firstName + " " + lastName;

        }

        [HttpPost]
        public string PostUsingBinding(Employee emp)
        {
            return "From Binding - " + emp.FirstName + " " + emp.LastName;

        }
    }
}