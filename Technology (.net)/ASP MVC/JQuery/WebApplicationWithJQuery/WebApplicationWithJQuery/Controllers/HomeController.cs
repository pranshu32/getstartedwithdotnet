﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplicationWithJQuery.Models;
using Newtonsoft.Json;

namespace WebApplicationWithJQuery.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetStudent()
        {
            Student std = new Student()
            {
                Id = 1,
                Email = "email@gmail.com",
                Name = "firstName lastName"
            };
            // using newtonsoft.Json; 
            var json = JsonConvert.SerializeObject(std); // converts the object to Json format!
            // can access using this ->
            // localhost:xxxx/home/GetStudent
            //"{\"Id\":1,\"Name\":\"firstName lastName\",\"Email\":\"email@gmail.com\"}"


            return Json(json, JsonRequestBehavior.AllowGet); // setting behaviour of Json by using this method
        }

        [HttpPost]
        public JsonResult AddStudent(Student student)
        {
            return Json("true", JsonRequestBehavior.AllowGet);
        }

    }
}