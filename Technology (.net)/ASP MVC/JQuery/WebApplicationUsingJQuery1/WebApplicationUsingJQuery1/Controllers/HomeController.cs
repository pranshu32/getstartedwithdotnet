﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplicationUsingJQuery1.Models;
using Newtonsoft.Json;

namespace WebApplicationUsingJQuery1.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(Employee model)
        {
            return View();
        }

        //[HttpPost]
        //public JsonResult Index(Employee model)
        //{
        //    return Json("true", JsonRequestBehavior.AllowGet);
        //}

        public ActionResult List()
        {
            return View();
        }
        
        public JsonResult Countries()
        {
            List<string> countries = new List<string>()
            {
                "Country1",
                "Country2",
                "Country3",
                "Country4",
                "Country5",
                "Country6",
                "Country7",
                "Country8",
                "Country9",
                "Country10"
            };
            var json = JsonConvert.SerializeObject(countries);
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public JsonResult City()
        {
            List<string> cities = new List<string>()
            {
                "City1",
                "City2",
                "City3",
                "City4",
                "City5",
                "City6",
                "City7",
                "City8",
                "City9",
                "City10"
            };
            var json = JsonConvert.SerializeObject(cities);
            return Json(json, JsonRequestBehavior.AllowGet);

        }

        public JsonResult States()
        {
            List<string> states = new List<string>()
            {
                "State1",
                "State2",
                "State3",
                "State4",
                "State5",
                "State6",
                "State7",
                "State8",
                "State9",
                "State10"
            };
            var json = JsonConvert.SerializeObject(states);
            return Json(json, JsonRequestBehavior.AllowGet);

        }
    }
}