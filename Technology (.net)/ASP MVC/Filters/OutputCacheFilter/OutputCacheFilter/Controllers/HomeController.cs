﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

//Variants - 
//[OutputCache(Duration = 20, Location = System.Web.UI.OutputCacheLocation.Any)]
//[OutputCache(Duration = 20, Location = System.Web.UI.OutputCacheLocation.Client)]
//[OutputCache(Duration = 20, Location = System.Web.UI.OutputCacheLocation.Downstream)]
//[OutputCache(Duration = 20, Location = System.Web.UI.OutputCacheLocation.Server)]
//[OutputCache(Duration = 20, Location = System.Web.UI.OutputCacheLocation.None)]
//[OutputCache(Duration = 20, Location = System.Web.UI.OutputCacheLocation.ServerAndClient)]

namespace OutputCacheFilter.Controllers
{
    //[OutputCache(Duration = 20)] // time is in seconds!, works on each actionmethod inside controller
    public class HomeController : Controller
    {
        // GET: Home
        //[OutputCache(Duration = 20)] // time is in seconds! // works for only this action method
        // once the page is loaded the page will only refresh after the duration is over! 
        // during the duration if you refresh the page it will show the content which loaded
        // when the page firstly appeared
        
        // Server
        //[OutputCache(Duration = 20, Location = System.Web.UI.OutputCacheLocation.Server)]
        //public ActionResult GetDate()
        //{
        //    return View();
        //}

        // Client
        [OutputCache(Duration = 20, Location = System.Web.UI.OutputCacheLocation.Client)]
        public ActionResult GetDate()
        {
            return View();
        }

    }
}