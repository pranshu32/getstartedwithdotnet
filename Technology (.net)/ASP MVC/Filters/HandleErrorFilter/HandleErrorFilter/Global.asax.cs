using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace HandleErrorFilter
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //filters.Add(new HandleErrorAttribute(), 2); //by default added  


            filters.Add(new HandleErrorAttribute
            {
                View = "CustomErrors"
            });
        }
        
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            RegisterGlobalFilters(GlobalFilters.Filters);
            //GlobalFilters.Filters.Add(new HandleErrorAttribute() { View = "CustomErrors" });
        }
    }
}
