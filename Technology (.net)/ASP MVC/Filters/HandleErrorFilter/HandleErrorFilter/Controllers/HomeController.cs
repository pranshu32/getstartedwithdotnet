﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HandleErrorFilter.Controllers
{
    [HandleError] // applies on all ActionMethods
    public class HomeController : Controller
    {
        // GET: Home
        //public ActionResult Index()
        //{
        //    try
        //    {
        //        throw new Exception("This is a Exception");
        //    }
        //    catch (Exception)
        //    {
        //        // redirects to error controller where the error is handled!
        //        return RedirectToAction("index", "Error");
        //    }
        //}

        // -----------------------------------------------------------------------------------

        // to overcome this of writing Try Catch Everywhere we use HandleError
        
        //[HandleError] // Will work only on this ActionMethod
        public ActionResult Index()
        {
            throw new Exception("This is a Exception"); // Explicitly raising error to see how it works!
        }

        public ActionResult About()
        {
            throw new Exception("This is a Exception"); // Explicitly raising error to see how it works!
        }
    }
}