﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.MyCustomProperty = "Names List -";
            ViewBag.MyList = new List<string>() {   "Pranshu", "Satyam", "Utkarsh", "Saujanya", "Ocean" };
            List<Employee> empList = new List<Employee>()
            {
                new Employee(){Email="a@gmail.com", Id=1, Name="Name1"},
                new Employee(){Email="b@gmail.com", Id=2, Name="Name2"},
                new Employee(){Email="c@gmail.com", Id=3, Name="Name3"},
                new Employee(){Email="d@gmail.com", Id=4, Name="Name4"}
            };
            ViewBag.empList = empList;
            return View();
        }
        public ActionResult About()
        {
            ViewData["Key"] = "ViewData List : ";

            ViewData["MyList"] = new List<string>() { "Pranshu", "Satyam", "Utkarsh", "Saujanya", "Ocean" };
            IList<Employee> empList = new List<Employee>()
            {
                new Employee(){Email="a@gmail.com", Id=1, Name="Name1"},
                new Employee(){Email="b@gmail.com", Id=2, Name="Name2"},
                new Employee(){Email="c@gmail.com", Id=3, Name="Name3"},
                new Employee(){Email="d@gmail.com", Id=4, Name="Name4"}
            };
            ViewData["empList"] = empList;
            return View();
        }
    }
}