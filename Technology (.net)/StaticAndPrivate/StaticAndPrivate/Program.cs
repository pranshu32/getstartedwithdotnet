﻿// static constructor - 
// its a special constructor that is called
// before the first object of the class is created.

// Used to initialize any static fields, or to perform
// a particular action that is need to perform only once.

// a class can have only one static
// constructor, having no access modifier.

using System;

namespace StaticAndPrivate
{
    public class Example
    {
        // Only static members are accessible using class name
        private static int Counter;
        // private constructor is not called automatically!
        private Example()
        {
            Counter = 10;
        }
        // static Constructor
        static Example()
        {
            Counter = 10;
        }
        public Example(int counter)
        {
            Counter += counter;
        }
        public static int GetCounter()
        {
            return ++Counter;
        }
        public class NestedExample
        {
            public void Test()
            {
                // internal Instance
                Example ex = new Example();
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            // external instance cant be created!
            // Example ex = new Example();
            Example ex = new Example(10);
            Console.WriteLine("Counter : {0}", Example.GetCounter());
        }
    }
}