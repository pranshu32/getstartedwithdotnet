﻿using System;

// in generics we create a class once
// and use it many times without facing any penalty!

// Converting value types to referrence types is known as boxing!
/*
namespace ConsoleApp1
{
    public class MainClass
    {
        private static void Main()
        {
            bool Equal = Calculator.AreEqual("A", "A");
            if (Equal)
            {
                Console.WriteLine("Equal");
            }
            else
            {
                Console.WriteLine("Not Equal");
            }
        }
    }
    public class Calculator
    {
        // due to object type boxing and unboxing
        //takes place  which reduces performance!
        public static bool AreEqual(object v1, object v2)
        {
            return v1 == v2;
        }
    }
}
*/
/*
namespace ConsoleApp1
{
    public class MainClass
    {
        private static void Main()
        {
            // here method is generic!
            // bool Equal = Calculator.AreEqual<string>("A", "A");
            bool Equal = Calculator.AreEqual<int>(10, 11);
            if (Equal)
            {
                Console.WriteLine("Equal");
            }
            else
            {
                Console.WriteLine("Not Equal");
            }
        }
    }
    public class Calculator
    {
        public static bool AreEqual<T>(T v1, T v2)
        {
            return v1.Equals(v2);
        }
    }
}
*/

namespace ConsoleApp1
{
    public class MainClass
    {
        private static void Main()
        {
            // here method is generic!
            // bool Equal = Calculator.AreEqual<string>("A", "A");
            bool Equal = Calculator<int>.AreEqual(10, 11);
            if (Equal)
            {
                Console.WriteLine("Equal");
            }
            else
            {
                Console.WriteLine("Not Equal");
            }
        }
    }
    // generic class!
    public class Calculator<T>
    {
        public static bool AreEqual(T v1, T v2)
        {
            return v1.Equals(v2);
        }
    }
}